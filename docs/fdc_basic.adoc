=== Sample request body

====
.O - Cycle sample account
----
5239141000000012 - CB05
5239141007377256 - CASD
0505749000000721
0417000000002110 - CASD or CRTC
0417002010900101 - CRTC
----
====

====
.M - Cycle sample for fs-ecs-data
for Synchrony (ClientName:"AAAA5361001")
[source,json]
----
{
	"common":{
		"accountId":"6020523201000530",
		"externalCustomerId":"N18078154253845536056455"
	},
	"electronicMailHomeAddressText":"taylor.greene@firstdata.com",
	"primaryCustomerSecondPhoneIdentifier":"555-555-5555"
}
----

.M - Cycle sample for fs-ecs-data
for Suntrust (ClientName:"AAAA5241001")
[source,json]
----
{
	"common":{
		"accountId":"5045440001009848",
		"externalCustomerId":"C17356135727051265012663"
	},
	"electronicMailHomeAddressText":"TAYLOR.greene@FIRSTDATA.COM"
}
----
====

=== Usable ClientName
Local testing:

* `harrison_cat` - M cycle
* `FFan-F2DDNDH` - M cycle
* `FFan-f2ddndh` - DataPower

ECS3 clients:
https://escmconfluence.1dc.com/display/NSA/ECS3+-+Client+URLs[ECS3 - Client URLs]

[NOTE]
====
* For the SSO-clients, their account information is not stored in ECS DB, retrieving SSO-clients account information will always return `null`. Only non-SSO-clients account information is stored in ECS DB.
====

=== Node login
 ssh newarch@r1dvap1057.1dc.com

[NOTE]
====
Don't skip ".1dc.com"!!!
====

=== Monit login
 Username: view 
 Password: view

=== Jenkins
* *TWO* Jenkins nodes
+
https://escmjenkins.1dc.com[Old Jenkins]
+
 Username: FDC LAN ID
 Password: FDC LAN PWD
+
https://nsadev.escmjenkins.1dc.com[New Jenkins]
+
 Username: FDC LAN ID
 Password: FDC LAN ID
+
[NOTE]
====
For the new Jenkins, you need to register by yourself.
====

* Modify Jenkins build configuration
+
Jenkins uses `Maven` to build target, so, in some cases, we need to modify the build parameter of 
`Maven`, we can do this follow the steps listed below.
+
** Step 01: Find out the build job that we want to modify the build configuration
from Jenkins Dashboard, click the build job name and enter the build job page.
+
image::images/Jenkins_job_dashboard.png[]

** Step 02: Click the `configure` item to enter the configuration page.
+
image::images/Jenkins_job_configuration.png[]

** Step 03: Click the `Build` tab at the top of the web-page, input build parameter in the
`Goal` field.
+
image::images/Jenkins_job_build_goal.png[]

* Jenkins build log
+
When Jenkins build fails, we need to read the Jenkins build log to get some useful information.
+
image::images/Jenkins_build_log.png[]

* Manually trigger Jenkins build
+
There are 2 ways to do this:
+
** Method 01: Go to Bitbucket, on the overview of the PR, trigger a Jenkins build.
+
image::images/Jenkins_build_bitbuckt.png[]

** Method 02: Go to Jenkins node, enter build job page, click `Build Now`.
+
image::images/Jenkins_build_Jenkins.png[]

* Change build executor
+
In Jenkins home page, on the left side of the website, there is a "Build Executor Status" column, all the executors are listed here.
+
image::images/jenkins_build_executor_status.png[]
+
For every build job, we can set its executor in the build job configuration.
+
image::images/Jenkins_job_dashboard.png[]
+
Find the `Label Expression` field
+
image::images/jenkins_set_executor.png[]

=== @OdsDate

When implement model class, String variables used to represent a date in the formant of:

 YYYYMMDD or YYYY-MM-DD

should be decorated with annotation `@OdsDate`.

When the date format is `YYYYMMDD`, the `@OdsDate` is used like this:

[source,java]
----
@OdsDate
private String validationDate;
----

When the date formant is `YYYY-MM-DD`, the `@OdsDate` is used like this:

[source,java]
----
@OdsDate(dateFormat = "YYYY-MM-DD")
private String validationDate;
----

[NOTE]
====
If used `@OdsDate` annotation in the model class, the test class that contains
methods used to retrieve variable annotated by `@OdsDate` must extend super class
`ClientDateFormatTest`.
This class is defined in package `com.firstdata.fs.acctmaintenance.resource`.
====

=== Some error message returned by ODS

* PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
+
The reason for this response from ODS is that the server changed their HTTPS SSL certificate, and our Java program cannot recognize the root certificate authority.
+
http://magicmonster.com/kb/prg/java/ssl/pkix_path_building_failed.html[A solution]
